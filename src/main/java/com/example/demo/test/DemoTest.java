package com.example.demo.test;

import com.example.demo.service.DistributeLockService;
import com.example.demo.thread.ThreadA;

/**
 * @类名: DemoTest
 * @描述: TODO
 * @作者: menghaowei
 * @创建时间: 2021/3/16
 * @版本: V1.0
 **/
public class DemoTest {

    public static void main(String[] args) {
        DistributeLockService distributeLockService = new DistributeLockService();
        for(int a= 0 ; a < 50 ; a++){
            ThreadA threadA = new ThreadA(distributeLockService);
            threadA.start();
        }
    }

}
