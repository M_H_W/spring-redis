package com.example.demo.thread;

import com.example.demo.service.DistributeLockService;

/**
 * @类名: ThreadA
 * @描述: TODO
 * @作者: menghaowei
 * @创建时间: 2021/3/16
 * @版本: V1.0
 **/
public class ThreadA extends Thread {

    private DistributeLockService distributeLockService;

    public ThreadA(DistributeLockService distributeLockService){
        this.distributeLockService = distributeLockService;
    }

    @Override
    public void run() {
        distributeLockService.seckill();
    }
}
