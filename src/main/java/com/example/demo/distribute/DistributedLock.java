package com.example.demo.distribute;

import com.example.demo.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.exceptions.JedisException;

import java.util.List;
import java.util.UUID;

/**
 * @类名: DistributedLock
 * @描述: 分布式锁的简单实现代码
 * @作者: menghaowei
 * @创建时间: 2021/3/16
 * @版本: V1.0
 **/
@Component
public class DistributedLock {

    /**
     *  思路：
     * 二， 基于缓存（Redis等）实现分布式锁
     *
     * 1、使用命令介绍：
     *
     * （1）SETNX
     *
     * SETNX key val：当且仅当key不存在时，set一个key为val的字符串，返回1；若key存在，则什么都不做，返回0。
     *
     * （2）expire
     *
     * expire key timeout：为key设置一个超时时间，单位为second，超过这个时间锁会自动释放，避免死锁。
     *
     * （3）delete
     *
     * delete key：删除key
     *
     * 在使用Redis实现分布式锁的时候，主要就会使用到这三个命令。
     *
     * 2、实现思想：
     *
     * （1）获取锁的时候，使用setnx加锁，并使用expire命令为锁添加一个超时时间，超过该时间则自动释放锁，锁的value值为一个随机生成的UUID，通过此在释放锁的时候进行判断。
     *
     * （2）获取锁的时候还设置一个获取的超时时间，若超过这个时间则放弃获取锁。
     *
     * （3）释放锁的时候，通过UUID判断是不是该锁，若是该锁，则执行delete进行锁释放。
     * **/
    @Autowired
    private JedisPool jedisPool;

    public DistributedLock(JedisPool jedisPool){
        this.jedisPool = jedisPool;
    }

    /**
     * 加锁
     * @param lockName       锁的key
     * @param acquireTimeout 获取超时时间
     * @param timeout        锁的超时时间
     * @return 锁标识
     */
    public String lockWithTimeout(String lockName, long acquireTimeout, long timeout){
        Jedis conn = null;
        String retIdentifier = null;
        try {
            //获取redis连接
            conn = jedisPool.getResource();
            //设置key的一个随机值
            String identifier = UUID.randomUUID().toString();
            //key的名称(锁的名称)
            String lockKey = "lock:" + lockName;
            //超时时间，上锁后超过此时间则自动释放锁
            int lockExpire = (int)(timeout / 1000);
            //获取锁的超时时间，超过这个时间则放弃获取锁
            long end = System.currentTimeMillis() + acquireTimeout;
            while (System.currentTimeMillis() < end){
                if(conn.setnx(lockKey,identifier) == 1){
                    conn.expire(lockKey, lockExpire);
                    // 返回value值，用于释放锁时间确认
                    retIdentifier = identifier;
                    return retIdentifier;
                }
                // 返回-1代表key没有设置超时时间，为key设置一个超时时间
                if(conn.ttl(lockKey) == -1){
                    conn.expire(lockKey, lockExpire);
                }
                try {
                    Thread.sleep(10);
                }catch (InterruptedException e){
                    Thread.currentThread().interrupt();
                }
            }
        }catch (JedisException e){
            e.printStackTrace();
        }finally {
            if(conn != null){
                conn.close();
            }
        }
        return retIdentifier;
    }

    /**
     * 释放锁
     * @param lockName   锁的key
     * @param identifier 释放锁的标识
     * @return
     */
    public boolean releaseLock(String lockName, String identifier){
        Jedis conn = null;
        boolean retFlag = false;
        try {
            String lockKey = "lock:" + lockName;
            conn = jedisPool.getResource();
            while (true){
                // 监视lock，准备开始事务
                conn.watch(lockKey);
                // 通过前面返回的value值判断是不是该锁，若是该锁，则删除，释放锁
                if (identifier.equals(conn.get(lockKey))){
                    Transaction transaction = conn.multi();
                    transaction.del(lockKey);
                    List<Object> results = transaction.exec();
                    if(results == null){
                        continue;
                    }
                    retFlag = true;
                }
                conn.unwatch();
                break;
            }
        }catch (JedisException e){
            e.printStackTrace();
        }finally {
            if(conn != null){
                conn.close();
            }
        }
        return retFlag;
    }

    /**
     * 问题：
     * 1、Q:什么是分布式锁？
     *   W:就是在控制分布式系统或者不同系统共享同一个资源的一种锁的实现，保证数据的一致性；
     *
     * 2、Q:分布式锁需要哪些条件（特性）？
     *   W: （1）、互斥性：在任意时刻，只有一个客户端持有锁；
     *      （2）、无死锁：在客户端持有锁的情况下，该客户端宕机或其他意外事件，能够释放锁，其他客户端仍然能够获取到锁；
     *      （3）、容错性：只要大部分redis节点活着，客户端就可以获取和释放锁；
     *
     * 3、Q:redis分布式锁，如果不加上过期时间，会出现什么问题？
     *   W:会出现死锁的情况，如客户端A获取到锁，但是突然因为外部原因（网络异常、停电、服务宕机。。。），此时锁一直不存在，客户端B，将无法获取到锁。
     *
     * 4、Q:加上过期时间，如果处理业务的时间大于redis key的过期时间怎么处理？
     *   W: 增加一个定时刷新过期时间的处理，如：开始一个线程，申明一个字段isOpenExpirationRenewal，且该字段要为原子性的，用volatile 修饰；在线程中刷新过期时间，同时在
     *      加锁的方法中，将isOpenExpirationRenewal设置为true，并开启线程，且此线程睡眠一定时间在执行；最后在释放锁的方法中将isOpenExpirationRenewal设置为false；
     *      样例代码如下：参照博客：https://www.cnblogs.com/fixzd/p/9479970.html
     *
     * 5、Q:加上过期时间，是一条redis命令处理，还是多条redis命令处理？多条会不会存在问题？
     *   W:一条；如果多条会存在单独设置过期时间的命令可能没有成功，则其他客户端可能无法得到这个锁；
     *
     *
     * 优化：
     * 如果出现了这么一个问题：如果setnx是成功的，但是expire设置失败，那么后面如果出现了释放锁失败的问题，那么这个锁永远也不会被得到，业务将被锁死？
     *
     * 解决的办法：使用set的命令，同时设置锁和过期时间
     *
     * set参数：
     *
     * set key value [EX seconds] [PX milliseconds] [NX|XX]
     * EX seconds：设置失效时长，单位秒
     * PX milliseconds：设置失效时长，单位毫秒
     * NX：key不存在时设置value，成功返回OK，失败返回(nil)
     * XX：key存在时设置value，成功返回OK，失败返回(nil)
     *
     * 实践：
     *
     * 127.0.0.1:6379> set unlock "234" EX 100 NX
     * (nil)
     * 127.0.0.1:6379>
     * 127.0.0.1:6379> set test "111" EX 100 NX
     * OK
     * ​
     * 这样就完美的解决了分布式锁的原子性；
     *
     * **/
}

