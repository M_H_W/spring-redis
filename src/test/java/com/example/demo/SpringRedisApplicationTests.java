package com.example.demo;

import com.example.demo.distribute.DistributedLock;
import com.example.demo.utils.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPool;

@SpringBootTest
class SpringRedisApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	StringRedisTemplate stringRedisTemplate;

	@Autowired
	RedisTemplate redisTemplate;

//	@Autowired
//	JedisPool jedisPool;

//	@Autowired
//	DistributedLock distributedLock;

	@Test
	public void testRedis(){
		// 测试redis
		if(stringRedisTemplate.hasKey("hello")){
			String hello = stringRedisTemplate.opsForValue().get("hello");
			System.out.println("从redis中获取 key-hello--value : "+hello);
			stringRedisTemplate.opsForValue().set("jane","is a boy");

		}
		redisTemplate.opsForValue().set("sysApk","sysAdfsgsgfdsgfdpk");
		Object sysApk = redisTemplate.opsForValue().get("sysApk");
		System.out.println(sysApk.toString());
	}

	int n = 500;

	public void seckill() {
		// 返回锁的value值，供释放锁时候进行判断
//		DistributedLock distributedLock = new DistributedLock();
//		String identifier = distributedLock.lockWithTimeout("resource", 5000, 1000);
//		System.out.println(Thread.currentThread().getName() + "获得了锁");
//		System.out.println(--n);
//		distributedLock.releaseLock("resource", identifier);
	}

	@Test
	public void testDistributeLock(){
		for(int a= 0 ; a < 50 ; a++){
			Thread thread = new Thread(new Runnable() {
															@Override
															public void run() {
//																seckill();
															}
													   }, "t" + a);
			thread.start();
		}
	}

	@Autowired
	RedisUtils redisUtils;

	@Test
	public void test001(){
		redisUtils.set("menghw","menghw_10");
	}
}
